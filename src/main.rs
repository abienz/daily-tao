extern crate reqwest;
extern crate victoria_dom;

use victoria_dom::DOM;

fn main()  -> Result<(), Box<dyn std::error::Error>>{
    let mut res = reqwest::get("http://dailytao.org")?;
    let text = res.text()?;
    let poem = DOM::new(&text.to_owned());
    let formatted_poem = String::from(poem.at("div.container p").unwrap().rtext_all());
    println!("{}\n", formatted_poem);

    Ok(())
}
